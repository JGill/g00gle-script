import time
from bs4 import BeautifulSoup
from urllib.request import urlopen, Request
from urllib.error import HTTPError


#gets google url and returns list of urls on google page, excludes tracking strings
def getPageLink(url):
    linkList=[]

    time.sleep(random()* 10)
    bsObj = getBsObj(url)

    for ref in bsObj.findAll("h3", {"class":"r"}):
        fullLink = (ref.find("a").get('href'))
        
        link = fullLink[fullLink.find("http"):fullLink.find("&",)]
        
        linkList.append(link)        
    return linkList


#gets google url list returns list of urls on all google pages
def getPageLinkList(urlList):
    bigList = []
    for i in urlList:
        try:
            bigList.extend(getPageLink(i))
        except:
            print ("one Miss\n\n")

    return bigList


#OverLoading doesn't work
'''
#takes google search url and number of pages . returns a list of url of the search google pages 
def genGoogleUrlList(googleUrl,pages):          # 3 means page 1 to 3
    googleUrlList = [googleUrl,]
    
    for i in range(10,(pages*10),10):
        newUrl = googleUrl + "&start=" + str(i)
        googleUrlList.append(newUrl)
    return googleUrlList
'''


#takes google search url and START and END page number. returns a list of all google pages 
def genGoogleUrlList(SearchTermOrUrl,startPages,endPages):           # 0,3 means page 1 to 3
    if 'www.google.' in SearchTermOrUrl:
        googleUrl = SearchTermOrUrl
    else:
        googleUrl = "http://www.google.com.au/search?hl=en&q=" + SearchTermOrUrl.replace(" ","+")

    googleUrlList = [googleUrl,]
    FirstPageExcludedList = []
    
    if startPages == 0:
        for i in range(10,(endPages*10),10):
            newUrl = googleUrl + "&start=" + str(i)
            googleUrlList.append(newUrl)
        return googleUrlList
    else:
        for i in range(((startPages)*10),(endPages*10),10):
            newUrl = googleUrl + "&start=" + str(i)
            FirstPageExcludedList.append(newUrl)
        return FirstPageExcludedList


#makes beautiful soup object
def getBsObj(url):
    
    try:
        html = urlopen(Request(url, headers={'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36'}))
    except HTTPError as e:
        print("Invalid URL Provided")
        return None
    try:
        bsObj = BeautifulSoup(html.read(),"lxml")
    except AttributeError as e:
        print("lxml CANT READ URL")
        return None
    return bsObj
